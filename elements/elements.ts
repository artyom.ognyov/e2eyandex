import {element, by} from "protractor";

export const YandexMainPageElements = {
	geolocationButton: element(by.css('.geolink__reg')),
	moreButton: element(by.className('home-link home-link_blue_yes home-tabs__link home-tabs__more-switcher dropdown2__switcher')),
	moreTabsPopup: element(by.className('home-tabs__more')),
	moreTabs: element.all(by.className('home-link home-tabs__more-link home-link_black_yes'))
};

export const YandexGeolocationPageElements = {
	cityField: element(by.className('input__control input__input')),
	cityFieldList: element.all(by.css('li.b-autocomplete-item'))
};