import {browser, ElementFinder, ExpectedConditions} from "protractor";

export class Waiters {
    public static async waitUntilElementIsDisplayed(webElement: ElementFinder) {
        try {
            await browser.wait(ExpectedConditions.visibilityOf(webElement), browser.params.waitWebElementMaxTimeout);
        } catch (err) {
            if (err.name === 'TimeoutError') {
                throw 'Истекло время ожидания, элемент не загрузился и не отображается';
            } else {
                throw err.name;
            }
        }
    }
}