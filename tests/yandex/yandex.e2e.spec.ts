import { browser, element, by } from "protractor";
import {YandexGeolocationPage, YandexMainPage} from "../../components/yandex";

describe('Yandex main page', async () => {

	beforeEach(async () => {
		await browser.waitForAngularEnabled(false);
	})


	it('Отображение вкладок "ещё" при смене локации', async () => {
		await YandexMainPage.getUrl();
		await YandexMainPage.clickGeolocationButton();
		await YandexGeolocationPage.changeCity("Лондон");
		await YandexMainPage.clickMoreButton();
		await YandexMainPage.getMoreTabsText();
		await YandexMainPage.clickGeolocationButton();
		await YandexGeolocationPage.changeCity("Париж");
		await YandexMainPage.clickMoreButton();
		await YandexMainPage.compareMoreTabsText();
	}, 150000)

})