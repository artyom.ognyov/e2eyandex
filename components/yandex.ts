import {browser, by} from "protractor";
import {YandexMainPageElements, YandexGeolocationPageElements} from "../elements/elements";
import { Waiters as w } from '../helpers/waiters';

export class YandexMainPage {
    private static yandexTabsText: any;

    public static async getUrl() {
        await browser.get("https://yandex.ru")
    }

    public static async clickGeolocationButton() {
        await YandexMainPageElements.geolocationButton.click();
    }

    public static async clickMoreButton() {
        await w.waitUntilElementIsDisplayed(YandexMainPageElements.moreButton);
        await YandexMainPageElements.moreButton.click();
    }

    public static async getMoreTabsText() {
        await w.waitUntilElementIsDisplayed(YandexMainPageElements.moreTabsPopup);
        YandexMainPage.yandexTabsText = await YandexMainPageElements.moreTabs.getText();
    }

    public static async compareMoreTabsText() {
        await w.waitUntilElementIsDisplayed(YandexMainPageElements.moreTabsPopup);
        expect(await YandexMainPageElements.moreTabs.getText()).toEqual(YandexMainPage.yandexTabsText);
    }
}

export class YandexGeolocationPage {
    private static async clickCityField() {
        await YandexGeolocationPageElements.cityField.click();
    }

    private static async fillCityField(cityName: string) {
        await YandexGeolocationPageElements.cityField.sendKeys(cityName);
    }

    private static async submitCityField() {
        await w.waitUntilElementIsDisplayed((YandexGeolocationPageElements.cityFieldList).get(0));
        await YandexGeolocationPageElements.cityFieldList.get(0).click();
    }

    private static async clearCityField() {
        await YandexGeolocationPageElements.cityField.clear();
    }

    public static async changeCity(cityName: string) {
        await this.clickCityField();
        await this.clearCityField();
        await this.fillCityField(cityName);
        await this.submitCityField();
    }
}